use super::{segment_hash, SegmentPage, SegmentPageRead, Segments};
use crate::{
    address::ADDRESS_PAGE_EXP,
    allocator::Allocator,
    config::Config,
    device::{FileDevice, Page, PageOps},
    id::SegmentId,
};
use std::sync::Arc;
use tempfile::Builder;

fn create_allocator(file_name: &str) -> Allocator {
    let file = Builder::new()
        .prefix(file_name)
        .suffix(".persy")
        .tempfile()
        .unwrap()
        .reopen()
        .unwrap();
    let config = Arc::new(Config::new());
    let disc = Box::new(FileDevice::new(file).unwrap());
    let (_, allocator) = Allocator::init(disc, &config).unwrap();
    allocator.allocate(5).unwrap(); //Just to be sure it not start from 0, it cannot happen in not test cases.

    allocator
}

#[test]
fn test_create_drop_segment() {
    let allocator = create_allocator("./raw_segment_create_delete.persy");
    let all = Arc::new(allocator);
    let root = all.allocate(ADDRESS_PAGE_EXP).unwrap();
    let root_index = root.get_index();
    Segments::init(root, &all).unwrap();
    all.disc_sync().unwrap();
    let mut segments = Segments::new(root_index, &all).unwrap();

    let seg = segments.create_temp_segment(&all, "some").unwrap();
    let id = seg.get_segment_id();
    segments.finalize_create_segment(seg);
    segments.flush_segments(&all).unwrap();
    assert!(segments.segments_id.contains_key("some"));
    assert!(segments.segments.contains_key(&id));
    segments.drop_segment("some");
    segments.flush_segments(&all).unwrap();
    assert!(!segments.segments_id.contains_key("some"));
    assert!(!segments.segments.contains_key(&id));
}

#[test]
fn test_create_close_drop_close_segment() {
    let allocator = create_allocator("./segment_pers_create_delete.persy");
    let all = Arc::new(allocator);
    let root = all.allocate(ADDRESS_PAGE_EXP).unwrap();
    let root_index = root.get_index();
    Segments::init(root, &all).unwrap();
    all.disc_sync().unwrap();
    let id;
    {
        let mut segments = Segments::new(root_index, &all).unwrap();
        let seg = segments.create_temp_segment(&all, "some").unwrap();
        id = seg.get_segment_id();
        segments.finalize_create_segment(seg);
        segments.flush_segments(&all).unwrap();
        all.disc_sync().unwrap();
    }
    {
        let mut segments = Segments::new(root_index, &all).unwrap();
        assert_eq!(segments.segments.len(), 1);
        assert!(segments.segments_id.contains_key("some"));
        assert!(segments.segments.contains_key(&id));
        segments.drop_segment("some");
        segments.flush_segments(&all).unwrap();
        all.disc_sync().unwrap();
    }
    {
        let segments = Segments::new(root_index, &all).unwrap();
        assert!(!segments.segments_id.contains_key("some"));
        assert!(!segments.segments.contains_key(&id));
    }
}

#[test]
fn test_create_close_drop_close_segment_off_page() {
    let allocator = create_allocator("./segment_pers_create_delete_off_page.persy");
    let all = Arc::new(allocator);
    let root = all.allocate(ADDRESS_PAGE_EXP).unwrap();
    let root_index = root.get_index();
    Segments::init(root, &all).unwrap();
    all.disc_sync().unwrap();
    {
        let mut segments = Segments::new(root_index, &all).unwrap();
        for i in 0..100 {
            let seg = segments.create_temp_segment(&all, &format!("some{}", i)).unwrap();
            segments.finalize_create_segment(seg);
        }
        segments.flush_segments(&all).unwrap();
        all.disc_sync().unwrap();
    }
    {
        let mut segments = Segments::new(root_index, &all).unwrap();
        for i in 0..100 {
            assert!(segments.segments_id.contains_key(&format!("some{}", i)));
            segments.drop_segment(&format!("some{}", i));
        }
        segments.flush_segments(&all).unwrap();
        all.disc_sync().unwrap();
    }
    {
        let segments = Segments::new(root_index, &all).unwrap();
        for i in 0..100 {
            assert!(!segments.segments_id.contains_key(&format!("some{}", i)));
        }
    }
}

#[test]
fn test_seg_insert_read_pointer() {
    let mut page = Page::new(vec![0; 1024], 0, 0, 10);
    let segment_id = SegmentId::new(0);
    page.segment_insert_entry(segment_id, 30, 10);
    let read = page.segment_read_entry(segment_id, 30);
    match read {
        Some(val) => assert_eq!(val.0, 10),
        None => assert!(false),
    }
}

#[test]
fn test_seg_insert_update_read_pointer() {
    let mut page = Page::new(vec![0; 1024], 0, 0, 10);
    let segment_id = SegmentId::new(0);
    page.segment_insert_entry(segment_id, 30, 10);
    page.segment_update_entry(segment_id, 30, 15);
    let read = page.segment_read_entry(segment_id, 30);
    match read {
        Some(val) => assert_eq!(val.0, 15),
        None => assert!(false),
    }
}

#[test]
fn test_seg_insert_delete_read_pointer() {
    let mut page = Page::new(vec![0; 1024], 0, 0, 10);
    let segment_id = SegmentId::new(0);
    page.segment_insert_entry(segment_id, 30, 10);
    page.segment_delete_entry(segment_id, 30);
    let read = page.segment_read_entry(segment_id, 30);
    match read {
        Some(_) => assert!(false),
        None => assert!(true),
    }
}

#[test]
fn test_hash_id_generator() {
    assert!(0 != segment_hash("some"));
}
