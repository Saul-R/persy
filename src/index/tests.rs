mod keeper;
mod serialization;

use crate::index::bytevec::ByteVec;
use std::sync::Arc;
#[test]
fn test_slice_ops() {
    let v = Arc::new(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    let b = ByteVec::new_slice(v, 2, 4);
    assert_eq!(vec![3, 4, 5, 6], Vec::from(b));
}
