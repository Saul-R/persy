use crate::{
    error::{GenericError, PERes, PIRes, ReadError},
    id::{IndexId, SegmentId},
    index::{
        config::{IndexTypeInternal, ValueMode},
        serialization::{deserialize, reuse_deserialize},
        tree::nodes::{Node, NodeRef, Nodes},
    },
    persy::PersyImpl,
    snapshots::SnapshotRef,
};
use std::rc::Rc;

pub trait IndexKeeper<K, V> {
    fn load(&self, node: &NodeRef) -> PERes<Node<K, V>>;
    fn load_with(&self, node: &NodeRef, reuse: Option<Nodes<K>>) -> PERes<Node<K, V>>;
    fn get_root(&self) -> PERes<Option<NodeRef>>;
    fn value_mode(&self) -> ValueMode;
    fn index_name(&self) -> &String;
}

pub trait IndexModify<K, V>: IndexKeeper<K, V> {
    fn load_modify(&self, node: &NodeRef) -> PIRes<Option<(Rc<Node<K, V>>, u16)>>;
    fn lock(&mut self, node: &NodeRef, version: u16) -> PIRes<bool>;
    fn owned(&mut self, node_ref: &NodeRef, node: Rc<Node<K, V>>) -> Node<K, V>;
    fn unlock(&mut self, node: &NodeRef) -> bool;
    fn get_root_refresh(&mut self) -> PIRes<Option<NodeRef>>;
    fn lock_config(&mut self) -> PIRes<bool>;
    fn insert(&mut self, node: Node<K, V>) -> PIRes<NodeRef>;
    fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>, version: u16) -> PIRes<()>;
    fn delete(&mut self, node: &NodeRef, version: u16) -> PIRes<()>;
    fn set_root(&mut self, root: Option<NodeRef>) -> PIRes<()>;
    fn bottom_limit(&self) -> usize;
    fn top_limit(&self) -> usize;
}

pub struct IndexSegmentKeeper<'a> {
    name: String,
    segment: SegmentId,
    root: Option<NodeRef>,
    store: &'a PersyImpl,
    snapshot: SnapshotRef,
    value_mode: ValueMode,
}

impl<'a> IndexSegmentKeeper<'a> {
    pub fn new(
        name: &str,
        index_id: &IndexId,
        root: Option<NodeRef>,
        store: &'a PersyImpl,
        snapshot: &SnapshotRef,
        value_mode: ValueMode,
    ) -> IndexSegmentKeeper<'a> {
        IndexSegmentKeeper {
            name: name.to_string(),
            segment: index_id.get_data_id(),
            root,
            store,
            snapshot: snapshot.clone(),
            value_mode,
        }
    }
}

impl<'a, K: IndexTypeInternal, V: IndexTypeInternal> IndexKeeper<K, V> for IndexSegmentKeeper<'a> {
    fn load(&self, node: &NodeRef) -> PERes<Node<K, V>> {
        let rec = self
            .store
            .read_snap_fn(self.segment, node, &self.snapshot, deserialize)
            .map_err(map_read_err)?
            .unwrap();
        Ok(rec)
    }
    fn load_with(&self, node: &NodeRef, reuse: Option<Nodes<K>>) -> PERes<Node<K, V>> {
        let rec = self
            .store
            .read_snap_fn(self.segment, node, &self.snapshot, |e| reuse_deserialize(e, reuse))
            .map_err(map_read_err)?
            .unwrap();
        Ok(rec)
    }
    fn get_root(&self) -> PERes<Option<NodeRef>> {
        Ok(self.root)
    }
    fn value_mode(&self) -> ValueMode {
        self.value_mode.clone()
    }

    fn index_name(&self) -> &String {
        &self.name
    }
}

pub(crate) fn map_read_err(r: ReadError) -> GenericError {
    match r {
        ReadError::SegmentNotFound => panic!("The segment should be already checked"),
        ReadError::InvalidPersyId(_) => panic!("The Internal id should be everytime valid"),
        ReadError::Generic(g) => g,
    }
}
